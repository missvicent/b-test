# BTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.3.

## Clone project 

Clone project from https://gitlab.com/missvicent/b-test.git

## Install

Run `npm install` after download it from repository.


## Run

Run `npm start` or `ng serve` after install all dependencies
