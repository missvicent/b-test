import {Component, OnInit, OnDestroy} from '@angular/core';
import {Service} from "../../services/service.service";
import {Subscription, timer} from 'rxjs';
import {switchMap} from "rxjs/internal/operators";
import {ToastrService} from "ngx-toastr";
import {Data} from "../../models/models";


@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.component.html',
  styleUrls: ['exchange.component.scss']
})
export class ExchangeComponent implements OnInit, OnDestroy {
  value:number=0;
  data:Data;
  selected:any;
  extra:any;
  global:any;
  date:any;

  constructor(private service: Service,private toastr: ToastrService) {}
  subscription: Subscription;

  ngOnInit() {
    this.getCurrentValue();
    this.data = new Data(0,0);
  }

  getCurrentValue(){
    this.subscription = timer(0, 13000000).pipe(
      switchMap(() => this.service.getCurrency('EUR'))
    ).subscribe(
      res=> {
        this.date=res.date;
        this.global = res.rates;
        this.extra = Object.keys(res.rates);
        this.selected = this.extra[0];
        this.value = res.rates[this.selected];
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  show1(){
    this.value=this.global[this.selected];
    this.data.change=this.data.current*this.value;
  }

  onSubmit(){
    this.toastr.success(`You dont need doing click to exchange. ${this.selected} ${this.data.change.toFixed(4)}!`);
  }

  change_rate(){
    this.data.change=this.data.current*this.value;
  }

}
