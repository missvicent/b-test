/**
 * Created by Nilyan Vicent on 15/12/2018.
 */
export class Currency{
  base: string;
  date: Date;
  rates:any;
  success: boolean;
  timestamp: number;
}

export class Data{
  current:number;
  change:number;

  constructor(change:number, current:number){
    this.change=change;
    this.current=current;
  }
}
