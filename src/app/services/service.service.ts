import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {Currency} from "../models/models";

@Injectable({
  providedIn: 'root'
})
export class Service{
  constructor(private http:HttpClient) { }

  getCurrency(base){
    return this.http.get<Currency>(environment.server+'&base='+base)
      .pipe(map(res => res));
  }

}
