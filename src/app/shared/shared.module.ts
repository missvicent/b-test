import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {NgxCurrencyModule} from "ngx-currency";
import {MatInputModule, MatIconModule} from "@angular/material";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {ToastrModule} from "ngx-toastr";
import {ExchangeComponent} from "../components/exchange/exchange.component";

@NgModule({
  declarations: [ExchangeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxCurrencyModule,
    ToastrModule.forRoot(),
    MatInputModule,
    MatFormFieldModule,MatSelectModule,MatButtonModule,MatIconModule
  ],
  exports:[
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxCurrencyModule,
    MatInputModule,
    MatFormFieldModule,MatSelectModule,MatButtonModule,MatIconModule,
    ExchangeComponent
  ]
})
export class SharedModule { }
